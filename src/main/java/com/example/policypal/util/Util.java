package com.example.policypal.util;

/**
 * Created by nsonanh on 27/05/2018
 */
public class Util {

    public static String convertStreamToString(java.io.InputStream is)
    {
        java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }
}
