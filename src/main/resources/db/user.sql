DROP TABLE IF EXISTS user;

CREATE TABLE user (
id int(11) unsigned NOT NULL AUTO_INCREMENT,
email varchar(50) DEFAULT NULL,
dob datetime DEFAULT NULL,
first_name varchar(11) DEFAULT NULL,
mobile_number varchar(11) DEFAULT NULL,
gender char(1) DEFAULT NULL,
marital_status varchar(11) DEFAULT NULL,
nationality varchar(11) DEFAULT NULL,
address varchar(256) DEFAULT NULL,
unit_no varchar(11) DEFAULT NULL,
postal varchar(11) DEFAULT NULL,
created datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
updated datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
PRIMARY KEY (id)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

LOCK TABLES user WRITE;

INSERT INTO user (id, email, dob, first_name, mobile_number, gender, marital_status, nationality, address, unit_no, postal, created, updated)
VALUES
(1,'TEST@TEST.COM','1987-02-22','TEST','81234567','M','SINGLE','OTHER','TEST ADDRESS','1','123','2017-02-22 00:00:00','2017-02-22 00:00:00');