package com.example.policypal.demopolicypal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@ComponentScan(basePackages = "com.example.policypal")
@EnableJpaRepositories(basePackages = "com.example.policypal.repo")
@EntityScan(basePackages = "com.example.policypal.model")
@EnableScheduling
public class DemoPolicypalApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoPolicypalApplication.class, args);
	}
}
