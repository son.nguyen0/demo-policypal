package com.example.policypal.repo;

import com.example.policypal.model.Registration;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by nsonanh on 24/05/2018
 */
@Repository
public interface RegistrationRepository extends JpaRepository<Registration, Long> {
}
