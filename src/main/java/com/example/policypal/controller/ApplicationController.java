package com.example.policypal.controller;

import com.example.policypal.model.Gender;
import com.example.policypal.model.Registration;
import com.example.policypal.model.User;
import com.example.policypal.model.WardType;
import com.example.policypal.repo.RegistrationRepository;
import com.example.policypal.repo.UserRepository;
import com.example.policypal.util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Date;
import java.text.SimpleDateFormat;

/**
 * Created by nsonanh on 23/05/2018
 */
@RestController
@RequestMapping("/api")
public class ApplicationController {

    private String rinkebyKey = "fhWfqH7w6E4o6tIlQenT";
    private String walletFilePassword = "policypal";

    private String rinkebyUrl = "https://rinkeby.infura.io/" + rinkebyKey;

    private String walletId = "abb5526850f8ba9dc33d1910d1a624afe70bcfd7";
    private String walletSource = "/home/son/testnet/keystore/UTC--2018-05-26T16-07-45.458000000Z--" + walletId + ".json";

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RegistrationRepository registrationRepository;

    @RequestMapping(value = "/user/{id}", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public User getUser(@PathVariable("id") Long id) {
        return this.userRepository.getOne(id);
    }

    @RequestMapping(value = "/registration/{id}", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public Registration getRegistration(@PathVariable("id") Long id) {
        return this.registrationRepository.getOne(id);
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<String> saveDetails(
            @RequestParam("email") String email,
            @RequestParam("birth_date") String dob,
            @RequestParam("given_name") String firstName,
            @RequestParam("mobile_number") String mobileNo,
            @RequestParam("gender") String gender,
            @RequestParam("marital_status") String maritalStatus,
            @RequestParam("citizenship") String nationality,
            @RequestParam("address") String address,
            @RequestParam("unit_no") String unitNo,
            @RequestParam("postal") String postal,
            @RequestParam("ward_type") String wardTypeName)
    {
        try {
            Date dateOfBirth = new Date(new SimpleDateFormat("dd/MM/yyyy").parse(dob).getTime());
            Gender genderEnum = Gender.fromGender(gender);
            User user = new User(email, dateOfBirth, firstName, mobileNo, genderEnum,
                    maritalStatus, nationality, address, unitNo, postal);

            Registration registration = new Registration(user, WardType.fromName(wardTypeName));

            this.userRepository.save(user);
            this.registrationRepository.save(registration);
            return ResponseEntity.status(HttpStatus.CREATED).build();
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }

    @RequestMapping(value = "/crypto", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<String> registerCryptoSmartContract(
            @RequestParam("input_source") String inputSource,
            @RequestParam("output_source") String outputSource,
            @RequestParam("amount") BigDecimal amount,
            @RequestParam("occasion") char occasionCode,
            @RequestParam("times") String times)
    {
        //        try {
        //            Occasion occasion = Occasion.fromCode(occasionCode);
        //            Web3j web3j = Web3j.build(new HttpService(rinkebyUrl));
        //
        //            //            Credentials credentials = WalletUtils.loadCredentials(
        //            //                    "<password>",
        //            //                    "/path/to/<walletfile>");
        //
        //
        //            Credentials credentials = WalletUtils.loadCredentials(walletFilePassword, walletSource);
        //
        //            Greeter contract = Greeter.deploy(web3j, credentials, ManagedTransaction.GAS_PRICE, Contract.GAS_LIMIT,
        //                    new String("Greetings to the blockchain world!")).sendAsync().get();
        //
        //            String greeting = contract.greet().sendAsync().get();
        return ResponseEntity.status(HttpStatus.OK).build();
        //        } catch (Exception e) {
        //            e.printStackTrace();
        //            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        //        }
    }

    @RequestMapping(value = "/crypto/{option}", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<String> registerCryptoSmartContract(@PathVariable("option") int option) throws IOException
    {
        String url = "https://api.etherest.io:8080/v1/main/0x4dc924EeB4D87ab938f5a72fC0ef4460f6b35a8A/getSandwichInfoCaloriesPrice/";
        String charset = "UTF-8";  // Or in Java 7 and later, use the constant: java.nio.charset.StandardCharsets.UTF_8.name()

        URLConnection connection = new URL(url + option).openConnection();
        connection.setRequestProperty("Accept-Charset", charset);
        InputStream response = connection.getInputStream();
        return ResponseEntity.ok(Util.convertStreamToString(response));
    }
}
