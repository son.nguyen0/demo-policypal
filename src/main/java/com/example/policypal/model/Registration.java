package com.example.policypal.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Date;

/**
 * Created by nsonanh on 24/05/2018
 */
@Entity
@Table(name = "registration")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Registration {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @ManyToOne
    @JoinTable(name = "user_registration", joinColumns = { @JoinColumn(name = "registration_id") }, inverseJoinColumns = { @JoinColumn(name = "user_id") })
    private User user;

    @Column(name = "ward_type")
    @Enumerated(EnumType.STRING)
    private WardType wardType;

    @Column(name = "yearly_amount")
    private BigDecimal yearlyAmount;

    @Column(name = "created")
    @CreationTimestamp
    private Date created;

    @Column(name = "updated")
    @UpdateTimestamp
    private Date updated;

    public Registration(User user, WardType wardType)
    {
        this.user = user;
        this.wardType = wardType;
        this.yearlyAmount = wardType.getAmount();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public WardType getWardType() {
        return wardType;
    }

    public void setWardType(WardType wardType) {
        this.wardType = wardType;
    }

    public BigDecimal getYearlyAmount() {
        return yearlyAmount;
    }

    public void setYearlyAmount(BigDecimal yearlyAmount) {
        this.yearlyAmount = yearlyAmount;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    @Override
    public String toString(){
        return String.format("Account[id=%d, name=%s]",id, user.getFirstName());
    }
}
